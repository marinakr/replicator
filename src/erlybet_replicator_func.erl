-module(erlybet_replicator_func).
-include("include/erlybet_replicator.hrl").
-include_lib("deps/dbproxy/include/dbproxy.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-export([
		 replicate_table/1,
		 replicate_table/2,
		 replicate_table/4,
		 data_pairs/3,
		 flat_all/1,
		 flat_all/2,
		 flat_all/3,
		 channel_rules/0,
		 check_trigger_exist/2,
		 check_fun_exist/3,
		 add_delimiter/2,
		 add_delimiter/1,
		 get_table_fields/3,
		 fill_config/1
		 ]).

-include_lib("helper/include/error_logger.hrl").

%% ====================================================================
%% MAIN
%% ====================================================================
replicate_table(TableS) ->
	replicate_table("public", TableS).
replicate_table(Schema, TableS) ->
	replicate_table(?DB_PRODUCTION, ?DB_ANALYTIC, Schema, TableS).
replicate_table(XDb, SDb, Schema, TableS) when is_list(TableS) andalso
												   is_list(Schema) andalso
												   is_atom(XDb) andalso
												   is_atom(SDb) andalso
												   SDb =/= XDb ->
%% 	TableD = TableS,
	ok.
%% 	case check_table_exist(SDb,Schema,TableD) of
%% 		not_exist ->
%% 			create_dest_table(XDb, SDb, Schema, TableS, TableD);	
%% 		exist -> 			
%% 			SourseFieldS = get_table_fields(XDb, Schema, TableS),
%% 			AdditionCols = reply_fiels_ok(XDb, Schema, TableS, SourseFieldS),
%% 			SourseFields = AdditionCols++SourseFieldS,
%% 			DestFields = get_table_fields(SDb, Schema, TableD),
%% 			AddDestFields = 
%% 				helper:mapfilter(fun({Name, Type}) ->
%% 										 case proplists:get_value(Name, DestFields) of
%% 											 undefined -> {Name, Type};
%% 											 _ -> skip
%% 										 end 
%% 								 end, 
%% 								 SourseFields),
%% 			case AddDestFields of 
%% 				[] -> ok;
%% 				[_|_] -> 
%% 					add_columns(SDb,Schema,TableD,AddDestFields)
%% 			end
%% 	end,
%% 	create_triger_replfields(XDb,Schema,TableS),
%%	erlybet_replicator_worker ! {add_table, Schema, TableS}.
		

%% ====================================================================
%% STEPS
%% ====================================================================	
add_to_config("",Size, DB = db_default) ->
	add_to_config("", DB, Size, 32, 15, 30);
add_to_config("",Size, DB = db_master) ->
	add_to_config("", DB, Size, 32, 30, 60);
add_to_config(Table,Size, DB = db_default) ->
	add_to_config(Table, DB, Size, 48, 15, 60);
add_to_config(Table, Size, DB = db_master) ->
	add_to_config(Table, DB, Size, 48,  5, 25).

add_to_config("", DB, Pool, PoolMax, Seconds, TSeconds) ->
	[get_env_field(H,DB, application:get_all_env(dbproxy)) || H <- 
	    [
			pg_host,
			pg_password,
			pg_user,
			pg_database,
			pg_timezone
		]
    ] ++
	[   
	   {pool_size, Pool},
	   {pool_exceeded_limit, PoolMax},
	   {peer_query_timeout_seconds, Seconds},
	   {peer_transaction_timeout_seconds,  TSeconds}
    ];
add_to_config(Table, DB, Pool, PoolMax, Seconds, TSeconds) ->
	[{name, helper:list_to_atom_safe(binary_to_list(iolist_to_binary([atom_to_list(DB)++"_"++Table])))}]++
		add_to_config("", DB, Pool, PoolMax, Seconds, TSeconds).
		

get_env_field(F,DB,Env) ->
	{F,  kvlists:get_path([databases, {name,DB}, F], Env)}.

check_trigger_exist(XDb,TriggerName) -> 
	case db:equery(XDb, "select 1::int from pg_trigger  where tgname = $1",
				   [TriggerName]) of 
		{ok,_,[{1}]} -> exist;
		{ok,_,[]} -> not_exist
	end.

check_fun_exist(XDb,Schema,TableS) ->
	case db:equery(XDb, " SELECT  1::int "
					    " FROM    pg_catalog.pg_namespace n " 
					    " JOIN    pg_catalog.pg_proc p "
					    " ON      pronamespace = n.oid "
					    " WHERE   nspname = $1 and proname= $2",
				    [Schema,TableS]) of 
		{ok,_,[{1}]} -> exist;
		{ok,_,[]} -> not_exist
	end.

check_table_exist(XDb,Schema,Table) -> 
	case db:equery(XDb, "select 1::int from information_schema.tables where table_schema = $1 and table_name = $2",
				   [Schema,Table]) of 
		{ok,_,[{1}]} -> exist;
		{ok,_,[]} -> not_exist
	end.
	
get_table_fields(XDb, Schema, TableS) ->
	{ok, _, TypeName} = db:equery(XDb, "SELECT '\"'||column_name||'\"', data_type "
			 "FROM information_schema.columns " 
			 "WHERE table_schema = $1 "
			 "AND table_name   = $2", [Schema, TableS]),
	TypeName.

add_columns(XDb,Schema,Table,Fields) ->
	TabBinary = list_to_binary(Schema++"."++Table),
	Struct = data_pairs(<<<<" ">>/binary>>,<<<<",add column ">>/binary>>, Fields),
	Txt  = flat_all(Struct, 
					<<
					  <<"begin; lock table ">>/binary, TabBinary/binary,<<";">>/binary,					  
					  <<"alter table ">>/binary, 
					  TabBinary/binary, <<" add column ">>/binary>>,
					<<<<";">>/binary
					,<<" commit;">>/binary
					>>
				   ),	
	?INFO("~nalter table ~p:~n~ts~n",[XDb, Txt]),
	{ok,[],[]} = db:equery(XDb,Txt,[]),
	Txt.	

reply_fiels_ok(XDb, Schema, TableS, TypeName) ->
	ColumnsNames = [Name || {Name, _} <- TypeName],	
	ReplyColumnsExists =  lists:map(fun({Name,Type}) ->
											{Name,Type, lists:member(list_to_binary(Name),ColumnsNames)} 
									end, 
									?REPLFIELDS),
	AddReplColList = helper:mapfilter(fun({_,_, true}) ->
											  skip;
										 ({Name,Type, false}) ->
											  {list_to_binary(Name),list_to_binary(Type)} 
									  end, ReplyColumnsExists),
	%?INFO("~nColumnsNames~n~p~n?REPLFIELDS~n~p~nReplyColumnsExists~n~p~nAddReplColList~n~p~n",[ColumnsNames,?REPLFIELDS,ReplyColumnsExists,AddReplColList]),
	AdditionCols = case AddReplColList of
					   [] -> [];
					   [_H|_T] ->
						   add_columns(XDb,Schema,TableS,AddReplColList),
						   AddReplColList
				   end,
	AdditionCols.
	

create_dest_table(XDb, SDb, Schema, TableS, TableD) ->
	TabBinary = list_to_binary(Schema++"."++TableD), 
	TypeName = get_table_fields(XDb, Schema, TableS),
	AdditionCols = reply_fiels_ok(XDb, Schema, TableS, TypeName),
	Struct = data_pairs(<<<<" ">>/binary>>,<<<<",">>/binary>>,
						AdditionCols++TypeName),
	CreateTableText  = flat_all(Struct, <<<<"CREATE TABLE ">>/binary, 
									  TabBinary/binary, <<"(">>/binary>>,<<<<");">>/binary>>),
	?INFO("~ncreate table ~p:~n~ts~n",[SDb, CreateTableText]),
	{ok,[],[]} = db:equery(SDb,CreateTableText,[]),
	{CreateTableText,Struct,TypeName}.

create_triger_replfields(_Xdb,_Schema,_Table) ->
    % TGName = "tg_"++Table++?REPLICA,
    % case check_trigger_exist(Xdb,TGName) of 
    %   not_exist -> 
    %       TGFun = 
    %           " CREATE OR REPLACE FUNCTION update_replica_fields() " 
    %           " RETURNS trigger AS "
    %           " $BODY$ "
    %           " DECLARE status_t integer; "
    %           " BEGIN " 
    %           " IF (TG_OP = 'UPDATE') THEN "
    %           " status_t = 0; "
    %           " ELSIF (TG_OP = 'INSERT') THEN "
    %           " status_t = 1; "
    %           " END IF; "
    %           " NEW.\"status_t\"=status_t; "
    %           " NEW.\"last_update\" = now(); "++
    %               case Xdb of 
    %                   ?DB_PRODUCTION ->" NEW.\"idn_erlyrepl\" = nextval(TG_TABLE_NAME||'_erlyrepl');";
    %                   ?DB_ANALYTIC -> ""
    %               end++                       
    %           " RETURN NEW; " 
    %           " END; "
    %           " $BODY$ "
    %           " LANGUAGE plpgsql VOLATILE "
    %               " COST 100;",
    %       TG = "CREATE TRIGGER "++TGName++" BEFORE INSERT OR UPDATE  ON "++
    %                Schema++"."++Table++
    %                " FOR EACH ROW EXECUTE PROCEDURE update_replica_fields();",
    %       %?INFO("~nTRIGGERS ~p :~n~ts~n~n~ts~n",[Xdb, TGFun, TG]),
    %       %db:equery(Xdb, "create sequence "++Table++"_erlyrepl;",[]),
    %       %{ok,[],[]} = db:equery(Xdb, TGFun, []),
    %       %{ok,[],[]} = db:equery(Xdb, TG, []),
    %       ok;
    %   exist -> ok
    % end,
    % IdxF = case Xdb of 
    %          ?DB_PRODUCTION -> "last_update";
    %          ?DB_ANALYTIC -> "idn"
    %      end,
    % Indx = "CREATE INDEX CONCURRENTLY "++
    %          "indx_"++Table++"_"++IdxF++" ON "++Schema++"."++Table++" ("++
    %                     IdxF++" DESC)",
    % %db:equery(Xdb, Indx, []),
    % %?INFO("~nINDEX ~p :~n~ts~n",[Xdb, Indx]),
    % %db:equery(Xdb, "INSERT INTO replica.reply_tables(tschema,tname) values ($1, $2) ", [Schema,Table]),
	ok.



%%======================================================================
add_delimiter(P) when is_list(P) ->
	add_delimiter(P,<<<<"">>/binary>>).
add_delimiter([L],_) -> [L];
add_delimiter([H|L],D) ->
	[<<H/binary,D/binary>> | add_delimiter(L,D)].

flat_all(L) ->
	flat_all(L, <<<<"">>/binary>>).
flat_all([],Acc) -> Acc;
flat_all([H|T],Acc) ->
	flat_all(T,<<Acc/binary,H/binary>>). 
flat_all([L],Acc,EndAll) -> 
	<<Acc/binary,L/binary,EndAll/binary>>;
flat_all([H|T],Acc,EndAll) ->
	flat_all(T,<<Acc/binary,H/binary>>,EndAll). 

data_pairs(Delimiter,_EndOfLine,[{Col, Type}]) ->
				   [<<Col/binary, Delimiter/binary, Type/binary>>];
data_pairs(Delimiter,EndOfLine,[{Col, Type}|Rest]) -> 
				   [<<Col/binary, Delimiter/binary, Type/binary, EndOfLine/binary>> | data_pairs(Delimiter,EndOfLine,Rest) ].

channel_rules() ->
	[?TGTABLENAME, ?TGTABLENAME++"_update"].


fill_config({NamePool,SizePool}) ->
	DBProxy = application:get_all_env(dbproxy),
	EDdb = proplists:delete(databases, DBProxy),
	TablesDB = 	 [add_to_config(NamePool,SizePool, db_master)]++
				 [add_to_config(NamePool,SizePool, db_default)],
	Databases = {databases, TablesDB},
	Proxy = EDdb++[Databases],
	DbChilds = lists:map(
				 fun( Db )->
						 { Db#db_state.name, { db, start_link, [[Db]] }, permanent, 5000, worker, [db] } 
				 end,
				 parce_env(Proxy)),
	R = lists:map(
		  fun(H) ->
				  %% =======================NEW POOL FOR ONE TABLE=============================%%
		          supervisor:start_child(dbproxy_sup, H)  
		  end,
		  DbChilds),
	R.


%% ====================================================================
%% DB_PROXY_CONF API 
%% ====================================================================	
parce_env(Env) ->
    case proplists:get_value(databases, Env) of
        undefined -> [pl2db_state(Env)];
        []        -> erlang:error({error, "Bad config data"});
        Databases ->
			parce_database_states( pl2db_state(Env), Databases, [] )
    end.

parce_database_states( _, [], Res ) ->
    Res;
parce_database_states( DefaultState, [DbProperties | Rest], Res ) ->
    parce_database_states( DefaultState, Rest, [pl2db_state(DbProperties, DefaultState) | Res ] ).

pl2db_state(L) ->
     pl2db_state(L, #db_state{}).

pl2db_state(L, DefaultState) when is_list(L) ->
    {_, DbState} = lists:foldl( 
        fun
            (_, {[], S}) ->
                {[], S};
            (Var, {PL, S}) ->
                case proplists:get_value(Var, PL) of 
                    undefined -> {PL, S};
                    V         -> {proplists:delete(Var, PL), pl2db_state(Var, V, S)}
                end
        end, {L, DefaultState}, ?CONFIG_VARS),
    DbState.


pl2db_state( _Var, undefined, State) -> State;
pl2db_state(name, V, State) -> State#db_state{name = V};
pl2db_state(master, V, State) -> State#db_state{master = V};
pl2db_state(slaves, V, State) -> State#db_state{slaves = V};
pl2db_state(pool_size, V, State) -> State#db_state{pool_size = V};
pl2db_state(pool_exceeded_limit, V, State) -> State#db_state{pool_exceeded_limit = V};
pl2db_state(pg_host, V, State) -> State#db_state{pg_host = V};
pl2db_state(pg_port, V, State) -> State#db_state{pg_port = V};
pl2db_state(pg_user, V, State) -> State#db_state{pg_user = V};
pl2db_state(pg_password, V, State) -> State#db_state{pg_password = V};
pl2db_state(pg_timezone, V, State) -> State#db_state{pg_timezone = V};
pl2db_state(pg_database, V, State) -> State#db_state{pg_database = V};
pl2db_state(close_unused_connection_after_seconds, V, State) -> State#db_state{unuse_timeout = V * 1000000};
pl2db_state(reconnect_every_seconds, V, State) -> State#db_state{live_timeout = V * 1000000};
pl2db_state(peer_transaction_timeout_seconds, V, State) -> State#db_state{trans_timeout = V * 1000000};
pl2db_state(peer_query_timeout_seconds, V, State) -> State#db_state{query_timeout = V * 1000}.





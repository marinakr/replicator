
-module(erlybet_replicator_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
	%io:format("~n~p~n",[application:get_all_env()]),
	Redis = {redo, {redo, start_link, [redis, redis_config()]}, permanent, 5000, worker, [redo]},
	Worker = ?CHILD(erlybet_replicator_worker, worker),
	Statistic = ?CHILD(erlybet_replicator_stats_client, worker),
    {ok, { {one_for_one, 500, 25}, 
		   [Redis, 
			Worker, Statistic ]} }.
redis_config() ->
	RedisHost = apputils:envdef(erlybet_replicator, redis_host, "default.favorit"),
	RedisPort = apputils:envdef(erlybet_replicator, redis_port, 0000),
	[{host, RedisHost}, {port, RedisPort}].

%% @author marina
%% @doc @todo Add description to erlybet_replicator_www.


-module(erlybet_replicator_www).
-record(state, {}).
-include_lib("helper/include/error_logger.hrl").
-include("include/erlybet_replicator.hrl").
-include("include/erlybet_replicator_sql.hrl").

%% API
-export([start/1, stop/0 %,loop/2
		]).
-export([handle_request/3, parse/1, init/3,terminate/3,handle/2]).

-define(VAL(Key, Params), proplists:get_value(Key, Params)).

%%====================================================================
%% API
%%====================================================================
start(Options) ->
	?INFO("Start Pay Terminals REST API with Options:~n~p~n",[Options]),
	application:start(crypto),
	application:start(public_key),
	application:start(ssl), 
	apputils:ensure_started(crypto),
	apputils:ensure_started(cowlib),
	apputils:ensure_started(ranch),
	apputils:ensure_started(cowboy),
	IPRaw     = apputils:envdef(favoritpay_mng, listen, {0,0,0,0}),
	Port      = apputils:envdef(favoritpay_mng, port, 8088),
	Path      = list_to_binary(normalize_path(apputils:envdef(favoritpay_mng, rest_path, "/"))),
	StartConn = apputils:envdef(favoritpay_mng, rest_connections_pool, 8),
	MaxConn   = apputils:envdef(favoritpay_mng, rest_max_connections, 1024),
	Compress  = apputils:envdef(favoritpay_mng, rest_compress, true),
	RestOptions = apputils:envdef(favoritpay_mng, rest_options, []),
	{ok, IP}  = host_to_ip(IPRaw),
	DispatchConf = [
					{'_', [ {<<Path/binary, <<"[...]">>/binary>>, favoritpay_www, RestOptions} ]}
				   ],
	Dispatch = cowboy_router:compile(DispatchConf),
	M = {ok, _} = cowboy:start_http(http, StartConn,
								[{ip, IP},
								 {port, Port},
								 {max_connections, MaxConn}],
								[{compress, Compress},
								 {env, [{dispatch, Dispatch}]}]
							   ),
	?INFO("~nSTART~n",[]),
	M.

normalize_path(Path) ->
    case lists:last(Path) of
        $/ -> Path;
        _ -> Path ++ "/"
    end.

host_to_ip(Host) ->
    case inet:getaddr(Host, inet6) of
        {ok, Addr} -> {ok, Addr};
        {error, _} -> inet:getaddr(Host, inet)
    end.
		  

stop() ->
	{ok,-1}.

init(_Transport, Req, _Options) ->
     {ok, Req, #state{}}.


handle(Req, _State) ->
	{PathBin, Req2} = cowboy_req:path(Req),
    {Method, Req3} = cowboy_req:method(Req2),
    Path = tl(binary_to_list(PathBin)),
	?INFO("~nReq3:~n~p~nMethod:~n~p~nPath:~n~p~n",[Req3, Method, Path]),
    try
        R = handle_request(Method, Req3, Path),
		?INFO("Favorit reply ~p",[R]),
		R
    catch
        Type:What ->
            Stacktrace = erlang:get_stacktrace(),
            {QSVals, Req4} = cowboy_req:qs_vals(Req3),
            {ok, BodyQS, Req5} = cowboy_req:body_qs(Req4),
            ?ERR("Web request failed:~n~p~n~p~n~p~n~p~n~p~n~p~n~p~n~p~n",[
                {method, Method},
                {path, Path},
                {type, Type},
                {what, What},
                {req, Req3},
                {qs, QSVals},
                {post_data, BodyQS},
                {stacktrace, Stacktrace}
            ]),
            req_fail(Req5)
    end.

% terminate(terminate_reason(), cowboy_req:req(), state()) -> ok.
terminate(_Reason, _Req, _State) ->
    ok.

%%--------------------------------------------------------------------
%% Internal functions
%%--------------------------------------------------------------------
%make_respond(Page,ContentType,Content,Req) -> {ok,Req2,#state{}}.
make_respond(Page,ContentType,Content,Req) ->
	{ok,Req2} = cowboy_req:reply(Page, ContentType,
								  Content,
								 Req),
	{ok,Req2,#state{}}.

make_parse(Req) ->
	{L,_} = cowboy_req:qs_vals(Req),
	M = fun([{K,V}|T],[],R) -> 
				R(T,[{binary_to_list(K),binary_to_list(V)}],R);
		   ([{K,V}|T],[Ha|Ta],R) ->
				R(T,[{binary_to_list(K),binary_to_list(V)},Ha|Ta],R);
		   ([],Result,_) -> Result
		end,
	M(L,[],M).

handle_request('GET', Req, Path) ->
    process_get(Path, Req,
				make_parse(Req) 
			   );

handle_request('POST', Req, Path) ->
    process_post(Path, Req,
				 make_parse(Req) 
				);
handle_request( _, _Path, Req) ->
	?INFO("~nREQ DENY~n",[]),
    req_deny(Req).
	
process_post(P, Req, _ ) ->
    ?DBG("Unhandled post:~n~p~p",[P, Req]),
    req_not_impl(Req).



process_get("replicator/set_table_replication"++ _, Req, Params )  ->
	make_respond(200,
				 [{<<"Content-Type">>, <<"text/plain">>}],
				 process_www_request(Params),	 Req);

process_get(Path, Req, Params) ->
    ?WARN( "Probe of unsupported 'GET' request. Path:~p~nParams:~p~n", [Path,Params]),
    req_not_impl(Req).

%-------------------------------------------------------------------------------------------------------------
%    WWW REPLICATOR API
%-------------------------------------------------------------------------------------------------------------

process_www_request(Params) ->
    www_command( ?VAL("command", Params), Params).


% curl -v "http://localhost:1114/eplicator/set_table_replication?command=add&table="card_acc"&schema="account""

www_command("add", Params ) ->
	Table = ?VAL("table", Params),
	www_response([{ok,Table}]);

www_command( Cmd, _Params ) ->
    www_response([
          {"result", 404},
          {"comment", Cmd ++ " Not implemented jet" }
      ]).

www_error( _Params, ErrorCode, ErrorComm) ->
    www_response([
          {"result", ErrorCode},
          {"comment",  ErrorComm }
      ]).

 www_response(Data) -> lists:flatten([
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", $\n,
		     "<response>", $\n,
        [ [$<,K,$>,erlybet_replicator_worker:bin(V),$<,$/,K,$>,$\n] || {K,V} <- Data ],
        "</response>"
    ]).

req_not_impl(Req) ->
	make_respond(500,
				 [{<<"Content-Type">>, <<"text/plain">>}],
				 "not implemented, sorry\n",
						 Req).

req_fail(Req) ->
	make_respond(500,
				 [{<<"Content-Type">>, <<"text/plain">>}],
				 "request failed, sorry\n",
						 Req).
    
req_deny(Req) ->
	make_respond(501,[],"Req Deny", Req).



%%====================================================================
%% Internal functions
%%====================================================================
simplexml_read_string([]) ->
        [];

simplexml_read_string(Str) ->
        Options = [{space,normalize},{encoding,"utf-8"}],
        try xmerl_scan:string(Str,Options) of
                {XML,_Rest} ->
                        xmerl_lib:simplify_element(XML)
        catch
                E:Error ->
                        io:format("### Error: xmerl_scan:string: ~p:~p~n",[E,Error]),
                        []
        end.

strip_whitespace({El,Attr,Children}) ->
        NChild = lists:filter(fun(X) ->
                case X of
                        " " -> false;
                        _   -> true
                end
        end,Children),
        Ch = lists:map(fun(X) -> strip_whitespace(X) end,NChild),
        {El,Attr,Ch};

strip_whitespace(String) -> String.

parse(Str) ->
        strip_whitespace(simplexml_read_string(Str)).




 -module(erlybet_replicator_fsm).
-behaviour(gen_fsm).
 
%% public API
-export([start/1, start_link/1]).
%% gen_fsm callbacks
-export([init/1, 
		 handle_event/3, 
		 handle_sync_event/4, 
		 handle_info/3,
		 terminate/3, 
		 code_change/4,
		 % custom state names
		 do_sync/3,
		 fill_config/2,
		 ask_master_rows/2
		]).


-include_lib("helper/include/error_logger.hrl").
-include("include/erlybet_replicator.hrl").
-include("include/erlybet_replicator_sql.hrl").

-record(fsm_config,	{table_name = "",
					 dest_schema = "",
					 dest_table = "",
					 primary_keys = [],
					 db_master = no,
					 db_default = no,
					 global_columns = [],
					 replica_delay = ?REPLICA_DELAY
					 }).

-define(WORKER, erlybet_replicator_worker).
-define(FUNCTIONS, erlybet_replicator_func).
-define(MAX_TIMEOUT, 30*12*?REPLICA_DELAY).
-define(FATAL_ERROR, fatal_error).

handle_info(go, StateName, State) ->
	self() ! ask_slave,
    {next_state, StateName, State};
handle_info(continue, StateName, State) ->
	timer:sleep(1000*3600),
	self() ! go,
	{next_state, StateName, State};
handle_info({?FATAL_ERROR, Error}, StateName, State = #fsm_config{dest_schema = Schema,  dest_table = Table }) ->
	?INFO("Saved signal ~p ~nStateInfo~n:Shema: ~p~nTable: ~p",[Error, Schema, Table]),
	timer:sleep(1000*3600),
	self() ! {?FATAL_ERROR, Error},%%!!!!!!!! TO DO
    {next_state, StateName, State};
handle_info(E, StateName, State) ->
	{_,Reply,_,_} = do_sync(E, self(), State), 
	{TimeOut, NextEvent} = result_handle(Reply),
	erlang:send_after(TimeOut, self(), NextEvent),
    {next_state, StateName, State}.

handle_sync_event(Event, From, StateName, State) ->
	?INFO("Event, From, StateName, State ~p",[{Event, From, StateName, State}]),
	Reply =  StateName(Event,From,State),
	?INFO("Reply ~p",[Reply]),
    {reply, Reply, StateName, State}.
handle_event(_ErrorMsg, _StateName, State) ->
    {ok, State}.

init( [{NameCh,NewTableConf}]) ->
	erlang:send_after(1000, NameCh, go),
	{ok, do_sync, fill_config(#fsm_config{}, NewTableConf), hibernate}.

%%====================== HANDLE DB RESULTS ========================%
result_handle({ask_slave, {ok, _, [{LU}]}}) -> {0, {ask_master, 0, LU}};
result_handle({{ask_master, Delay, _Lu}, {ok,_,[]}}) -> {Delay, ask_slave};
result_handle({{ask_master, _, _Lu}, R = {ok,_,[_|_]}}) -> {0, {save_to_redis, R}};

result_handle({{save_to_redis, E}, fail}) -> {?REPLICA_DELAY, {save_to_redis,E}};
result_handle({{save_to_redis, _}, ok}) -> {0, {write_slave_insert, ?MAXLIMIT}};

result_handle({{write_slave_insert, Limit, T}, {ok,V,_,_}})  when V > 0 -> 
	[red:lpop(?REDIS_KEY_INSERT++T) || _K <- lists:seq(1, Limit, 1)],
	{0, {write_slave_insert, ?MAXLIMIT}};
result_handle({{write_slave_insert, Limit, T}, ?ERR_EMPTY}) -> %%payment_history
	[red:lpop(?REDIS_KEY_INSERT++T) || _K <- lists:seq(1, Limit, 1)],
	{0, {write_slave_insert, 1}};
result_handle({{write_slave_insert, 1, T},?ERR_PGSQL_KEY_VIOLATES}) ->
	red:lpop(?REDIS_KEY_INSERT++T), 
	{0, {write_slave_insert, ?MAXLIMIT}};
result_handle({{write_slave_insert, _, _},?ERR_PGSQL_KEY_VIOLATES}) -> 
	{0, {write_slave_insert, 1}};
result_handle({{write_slave_insert, _Limit, _}, {time_out, 0} }) -> 
	{0, check_row_exist};



result_handle({{check_row_exist,_}, empty}) -> {0, ask_slave};
result_handle({{check_row_exist, T}, {Row,{ok,_,[]}}}) -> 
	red:lpush(?REDIS_KEY_INSERT++T, Row), 
	{0, {write_slave_insert, 1}};
result_handle({{check_row_exist,T}, {Row,{ok,_,[{1}]}}}) -> 
	{0, {write_slave_update, Row, T}};


result_handle({{write_slave_update, _, T}, ?ERR_EMPTY}) -> %%account.payment_history
	red:lpop(?REDIS_KEY_UPDATE++T),
	{0, {write_slave_insert, 1}};
result_handle({{write_slave_update, _, T}, {ok,_,_,_}}) -> 
	red:lpop(?REDIS_KEY_UPDATE++T),
	{0, check_row_exist};

result_handle({Event, {time_out, TimeOut}}) -> {TimeOut,Event};
result_handle({Event, ?ERR_PGSQL_KEY_VIOLATES = Err}) ->
	?ERR("Replicator: Key violate ~p",[Event]),
	{0, {?FATAL_ERROR,  Err}};
result_handle({_Event,  {error,{error,error, <<"4",_/binary>> = ErrCode, ErrMess, _}} = Error }) -> 
	?ERR("Replicator Syntax Error! Fix it in database! ~p",[[ErrCode,ErrMess]]),
	{0, {?FATAL_ERROR, Error}};
result_handle({Event, ?ERR_FOREIGN_KEY_VIOLATES}) -> {30*12*?REPLICA_DELAY,Event};
result_handle({Event, ?ERR_OVERLOAD}) -> {12*?REPLICA_DELAY,Event};
result_handle({Event, ?ERR_PGSQL_STATEMENT_TIMEOUT}) -> {?REPLICA_DELAY,Event};
result_handle({Event, ?ERR_COMM_TIMEOUT}) -> {?REPLICA_DELAY,Event};
result_handle({Event, ?ERR_TIMEOUT}) -> {?REPLICA_DELAY,Event};
result_handle({Event, Error}) -> 
	?INFO("result_handle error ~p",[[Event,Error]]),
	{13*?REPLICA_DELAY,Event}.


%%======= SINGLE STATE  DO_SYNC HANDELING EVENTS SYNCHRONIC=========%
do_sync(Event = ask_slave, _From, StateData = #fsm_config{db_default = DB, dest_table = T}) ->
	SQL = ?ASK_MAX_LAST_UPDATE++T,
	Reply = db:equery(DB,SQL,[]),
	%{stop, normal, State};
	{reply,{Event,Reply},do_sync,StateData};

do_sync({ask_master, _, LU}, _From, 		
		StateData = #fsm_config{db_master = DB, dest_table = T, global_columns = Columns,replica_delay = Delay}) ->
	SQL = ask_master_rows(T,Columns),
	Reply = db:equery(DB,SQL,[LU,?MAXLIMIT]),
	{reply,{{ask_master, Delay, LU},Reply},do_sync,StateData};

do_sync(Event = {save_to_redis, {ok,Fields,Records}}, _From, StateData = #fsm_config{dest_table = T}) ->
	try
		FieldsCount = length(Fields),
		[{?RECORD_INSERTED,Vi},{?RECORD_UPDATED,Vu}] = ?WORKER:sort_lists(Records),
		[is_integer(red:rpush(?REDIS_KEY_INSERT++T,  erlang:term_to_binary({FieldsCount,E}))) ||  E <- Vi],
		[is_integer(red:rpush(?REDIS_KEY_UPDATE++T,  erlang:term_to_binary({FieldsCount,E}))) ||  E <- Vu],	
		{reply, {Event, ok}, do_sync, StateData}
	catch What:Why ->
			  ?ERR("Error in write to redis: ~p ~p ~n", [What, Why]),
			  {reply, {Event, fail}, do_sync, StateData}
	end;

do_sync( {write_slave_insert, Limit, _}, From, StateData) ->
	do_sync({write_slave_insert, Limit}, From, StateData);
do_sync( {write_slave_insert, Limit}, _From, 
		StateData = #fsm_config{db_default = DB, dest_table = T, global_columns = Columns}) ->
	Reply = case red:lrange(?REDIS_KEY_INSERT++T, 0, Limit-1) of
				[] -> {time_out, 0};
				R = [FirstRow|_] ->	
					Records = [Rec || {_,Rec} <- [erlang:binary_to_term(H) || H <- R]],
					{FieldsCount, _} = erlang:binary_to_term(FirstRow),					
					{DB, SQL, Values} = ?WORKER:sync_inserts(DB, T, Columns, FieldsCount, Records),
					db:equery(DB, SQL, Values);
				Err ->
					?ERR("Err", [Err])
			end,
	{reply,{{write_slave_insert, Limit, T},Reply},do_sync,StateData};

do_sync({Event = check_row_exist,_}, From, StateData) ->
	do_sync(Event,From, StateData);
do_sync(Event = check_row_exist,_From, StateData = #fsm_config{db_default = DB, dest_table = T, 
															   primary_keys = Primary_keys,global_columns = Columns}) ->
	Reply = case red:lrange(?REDIS_KEY_UPDATE++T, 0, 0) of
				[] -> empty;
				[R] -> 
					Rr = {_FieldsCount, _Record} = erlang:binary_to_term(R),
					{DB, SQL, Values} = ?WORKER:sync_exists(DB, T, Columns, Rr, Primary_keys),
					{R, db:equery(DB, SQL, Values)}
			end,
	{reply, {{Event,T},Reply}, do_sync, StateData};

do_sync( {write_slave_update, R, _},From, StateData) ->
	do_sync( {write_slave_update, R},From, StateData);
do_sync({write_slave_update, R}, _From, StateData = 
			#fsm_config{db_default = DB, dest_table = T,
						global_columns = Columns,primary_keys = Primary_keys}) ->
	{FieldsCount, Record} = erlang:binary_to_term(R),
	{DB, SQL, Values} = ?WORKER:sync_updates(DB, T, Columns, FieldsCount, Record, Primary_keys),
	Reply = db:equery(DB, SQL, Values),
	R1 = {reply, {{write_slave_update, R, T},Reply}, do_sync, StateData},
	R1;


do_sync(Unknown, From, StateData) ->
	 ?INFO("~p",[[Unknown, From,  StateData]]),
     {rely, Unknown, do_sync, StateData}.


code_change(_OldVsn, StateName, State, _Extra) ->
	{ok, StateName, State}.
terminate(_Reason, _StateName, _State) ->
	ok.

fill_config(State=#fsm_config{},Attr) ->
	T = proplists:get_value(table_name, Attr),
	S = State#fsm_config{table_name = T,
						 dest_schema = proplists:get_value(dest_schema, Attr),
						 dest_table = proplists:get_value(dest_table, Attr),
						 primary_keys = proplists:get_value(primary_keys, Attr),
						 db_master = proplists:get_value(db_master, Attr),
						 db_default = proplists:get_value(db_default, Attr),
						 global_columns = proplists:get_value(global_columns, Attr),
						 replica_delay = proplists:get_value(replica_delay, Attr)},	
	S#fsm_config{}.


%-------------------------------
ask_master_rows(TabS,Columns) ->
	iolist_to_binary(["select ",?WORKER:txtcolumns(Columns)," from ", TabS, 
					  " where last_update > $1 and last_update < now()-interval'20 second' order by last_update limit $2 "]).

%% =======================		CALL  START		=============================%%
start({{local,Name},Args = {_Name,_Attr}}) ->
	start({{local,Name},Args,[]});
start({{local,Name},Args = {_Name,_Attr},OPts}) ->
	gen_fsm:start(Name, ?MODULE, Args, OPts).

start_link({Name, Args}) ->
	gen_fsm:start_link(Name, ?MODULE, [Args], []).


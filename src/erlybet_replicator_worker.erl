-module(erlybet_replicator_worker).

-behaviour(gen_server).

%% Application callbacks
-export([%start/2, 
		 stop/1]).

%% API
-export([start_link/0, unl_balance_out/1,unl_balance_in/1 ]).


%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
		 terminate/2, code_change/3]).

-export([add_new_table/5,
		 add_table/5,
		 bin/1,
		 global_columns/4,
		 %result_handle/3,
		 %result_handle/4,
		 sort_lists/1,
		 sync_inserts/5,
		 sync_exists/5,
		 sync_updates/6,
		 setfunc/4,
		 txtcolumns/1
		]).

-include_lib("helper/include/error_logger.hrl").
-include_lib("helper/include/helper.hrl").
-include("include/erlybet_replicator.hrl").
-include("include/erlybet_replicator_sql.hrl").

-record(state, {
				tables = [],
				stats = [],
				channels = [],
				diff = {add, {0,{0,0,0}}},
				xdb = ?DB_PRODUCTION,
				sdb = ?DB_ANALYTIC
			   }).

%% ===================================================================
%% Application callbacks
%% ===================================================================
start_link() ->
	gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).


stop(_State) ->
	ok.

init([]) -> 
	erlang:send_after(1000, ?SERVER, read_tables),
	%erlang:send_after(1000, ?SERVER, codes_check),
	{ok, #state{}}.

handle_info(read_tables,State) ->
	{noreply, read_table_list(State)};
handle_info( {result_handle, LastResult, Database, SQL, Params}, State) ->
	proc_lib:spawn(?MODULE, result_handle, [LastResult, Database, SQL, Params]),
	{noreply, State};
handle_info( {add_table, Schema, TableS, Pool,PoolSize}, State) ->
	{noreply,add_table(Schema, TableS, Pool, PoolSize, State)};
handle_info(update_stats,State) ->		
	proc_lib:spawn(?MODULE,calc_stats,[]),
	{noreply, State};

handle_info({make_sync, TableName}, State = #state{tables = Attr}) -> 
	proc_lib:spawn(fun () ->
							TableAttr = proplists:get_value(TableName,Attr),
							Fsm = proplists:get_value(fsm_name,TableAttr),
							gen_fsm:sync_send_event(Fsm, ask_slave, 30*12*?REPLICA_DELAY),
							erlang:send_after(  0, ?SERVER, {make_sync, TableName})
				   end),    
	{noreply, State};
handle_info({revise_table, Table, StartPeriod, EndPeriod},State) ->
	proc_lib:spawn(?MODULE, revise_table, [Table, StartPeriod, EndPeriod, State]),
	{noreply, State};
handle_info(state, State) ->
	?ERR("unhadled info ~p~n",[State]),
	{noreply, State};
handle_info(Info, State) ->
	?ERR("unhadled info ~p~n",[Info]),
	{noreply, State}.

handle_call(get_stats,_From,State) ->
	{reply, red_stat(), State};
handle_call({add_table, Schema, TableName, Pool, PoolSize}, _From, State) ->	
	NewState = add_table(Schema, TableName, Pool, PoolSize, State),
	{reply, ok,  NewState}.

code_change(_,_,_) -> ok.

handle_cast(_Info, State) ->
	{reply, ok, State }.

terminate(_,_) ->	
	ok.



%% ===================================================================
%% Insert Functions
%% ===================================================================
add_new_table(Schema,Table,Pool,PoolSize,Delay) ->
	case  db:equery(db_master,"select max(last_update) from "++Schema++"."++Table,[]) of
		{ok, _, [{{_B,_S}}]} ->
			erlybet_replicator_func:fill_config({Pool,PoolSize}),
			self() ! {add_table, Schema, Table, Pool, Delay},
			?INFO("Signan ~p saved. Check it",[[Schema,Table]]),
			ok;
		Error ->
			?INFO("No replication ~p, db error last_update in master ~p",[[Schema,Table],Error]),
			ok
	end.
	
	
read_table_list(State = #state{ xdb = XDb }) -> 
	case  db:equery(XDb,?POOLS,[]) of
		{ok, _, []} ->  
			?INFO("No one poll, no replica started",[]),
			State;
		{ok, _, Pools} ->
			%% =======================START DB_POOLS FOR TABLES===================%%
			[erlybet_replicator_func:fill_config(V) || V <- Pools],
			{ok, _, ListTables} =% {ok,_,[]},
				%{ok,_,[{<<"public">>,<<"card">>,<<"card_pool">>,5},{...}|...]}, 
				db:equery(XDb,?TABLES4REPLICATION,[]),
			case ListTables of
				[] -> ?INFO("Nothing to replicate",[]),
					  State;
				[_|_] ->
					Fs = fun(FState, [], _) -> FState;
							(St, [{Sch,Tbl,Pool,Delay}|Tail], R) ->
								 %% =======================START POOL&FSM FOR ONE TABLE===================%%
								 self() ! {add_table, binary_to_list(Sch), binary_to_list(Tbl), binary_to_list(Pool), Delay},
								 R(St, Tail, R)
						 end,
					Final = Fs(State, ListTables, Fs),			
					Final;
				Err -> ?ERR("Error in repl tables list~p",[Err]),
					   State
			end;
		Err -> ?ERR("Error in repl tables list~p",[Err]),
			   State
	end.


add_table(Schema,Table,Pool,Delay,State = #state{
									  tables = Tables,
									  xdb = XDb,
									  sdb = SDb									  
									 }) when is_list(Table) andalso
												 is_list(Schema) ->
	ConjoinedFields = global_columns(XDb, SDb, Schema, Table),
	case get_primary_key(XDb,Schema,Table) of
		error -> State;
		[] ->
			?INFO("No primary key in table ~p",[Schema++"."++Table]), 
			State;
		Primary = [_H|_] ->
			TableAdr = Schema++"."++Table,
			NameCh = erlang:list_to_atom(TableAdr),
			Values = [{fsm_name, NameCh},
					  {global_columns, ConjoinedFields},
					  {table_name, Table},
					  {dest_schema, Schema},
					  {dest_table, TableAdr},
					  {primary_keys, Primary},
					  {replica_delay, Delay},
					  {db_master, helper:list_to_atom_safe("db_master_"++Pool)},
					  {db_default, helper:list_to_atom_safe("db_default_"++Pool)}],
			NewTableConf = [{Schema++"."++Table,
							 Values}
						   ],
			%% =======================START FSM FOR ONE TABLE=============================%%
			Obj = {NameCh, {erlybet_replicator_fsm, 
							start_link, 
							[{ {local,NameCh},
							   {NameCh,Values}
							 }]}, 
				   permanent, 5000, worker, [erlybet_replicator_fsm]},
			{ok, _Pid} = supervisor:start_child(erlybet_replicator_sup, Obj), 
			EState = State#state{tables = Tables ++ NewTableConf },
			EState
	end.

global_columns(XDb,SDb,Schema,Table) ->
	SourseFieldS = erlybet_replicator_func:get_table_fields(XDb, Schema, Table),
	DestFieldS = erlybet_replicator_func:get_table_fields(SDb, Schema, Table),
	Sourse = [Name || {Name, _} <- SourseFieldS],
	Dest = [Name || {Name, _} <- DestFieldS],
	F1 = fun([], _, _) -> [];
			([H|T], L, R) -> 
				 [case lists:member(H, L) of 
					  true -> H; 
					  _ -> skip 
				  end
					  | R(T, L, R)] 
		 end,
	R = helper:mapfilter(
		  fun(skip) -> skip;
			 (H) -> H 
		  end, 
		  F1(Dest,Sourse,F1)),
	D1=  [<<<<"\"status_t\"">>/binary>>]++ lists:delete(<<<<"\"status_t\"">>/binary>>, R),
	D1.
	
get_primary_key(Xdb,Schema,Table) -> 
	case db:equery(Xdb, ?PK_KEYS, [Schema++"."++Table]) of
		{ok, _, Primary} ->  Primary;
		Err -> 
			?ERR("~nError get primary for table ~p ~n~p~n",[Schema++"."++Table,Err]),
			error
	end.

										 
sync_inserts(XDbS, TabDB, IColumns, S, R=[Head|_Tail] ) when is_tuple(Head) -> 
	Columns = param_number(S, R, 1),
	Insert = erlybet_replicator_func:flat_all(erlybet_replicator_func:add_delimiter(
												[erlybet_replicator_func:flat_all(H) || H <- Columns],
												<<<<",">>/binary>>)),
	IntersecColumns = txtcolumns(IColumns),
	Into = erlybet_replicator_func:flat_all(IntersecColumns),
	SQL = erlang:iolist_to_binary(["insert into ",TabDB,"(",Into,") values ",Insert,"  returning status_t::integer"]), 
	Params = myflat([erlang:tuple_to_list(H) || H <- R]),
	{XDbS, SQL, Params}.	

sync_exists(Db, TabDB, IColumns, {_, LV}, Primary_keys) when is_tuple(LV) ->
	Fm = [F || {F} <- Primary_keys],
	ListVals = tuple_to_list(LV),
	Lu = [fpk_fun(V, IColumns, ListVals) || V <- Primary_keys],
	Lpk = helper:mapfilter(fun(skip) -> skip; (H) -> H end,  lists:flatten(Lu)),
	Up1 = setfunc(Fm, 1, <<<<" ">>/binary>>, <<<<" and ">>/binary>>),
	SelectUpd = erlang:iolist_to_binary(["select 1::integer a from ",TabDB, " where ", Up1]),
	{Db, SelectUpd, Lpk}.


sync_updates(Db, TabDB, IColumns, S, LV, Primary_keys) when is_tuple(LV) ->
	Cols = setfunc(IColumns, 1, <<<<" ">>/binary>>, <<<<", ">>/binary>>),
	Fm = [F || {F} <- Primary_keys],
	UpdateWhere = setfunc(Fm, S+1, <<<<" ">>/binary>>, <<<<" and ">>/binary>>),
	ListVals = tuple_to_list(LV),
	Lu = [fpk_fun(V, IColumns, ListVals) || V <- Primary_keys],
	Lpk = helper:mapfilter(fun(skip) -> skip; (H) -> H end,  lists:flatten(Lu)),
	Params = ListVals++Lpk,
	SQL = erlang:iolist_to_binary(["update ",TabDB," set ",Cols," where ",UpdateWhere,"  returning 1::integer"]),
			{Db,SQL,Params}.

red_stat() ->
	{redis_data, [{Key,red:llen(Key)} || Key <- red:keys(<<"*">>)]}.


 
%% ===================================================================
%%							Code
%% ===================================================================
myflat(L) -> myflat(L,[]).
myflat([],Acc) -> Acc; 
myflat([H|T],Acc) -> myflat(T, H++Acc).
sort_lists(List) -> sort_lists([],[],List).
sort_lists(I,U,[]) ->  
	R = [{?RECORD_INSERTED,I},{?RECORD_UPDATED,U}],
	R;
sort_lists(I,U,[H|T]) ->
	case erlang:element(1,H) of
		?RECORD_INSERTED ->
			sort_lists([H]++I,U,T);
		?RECORD_UPDATED ->
			sort_lists(I,[H]++U,T)
	end.

fpk_fun(_, [],[]) -> [];
fpk_fun({Pk}, [Pk|TN],[V|TV]) -> [V | fpk_fun({Pk}, TN, TV)];
fpk_fun({Pkn}, [_|TN],[_|TV]) -> [skip | fpk_fun({Pkn}, TN, TV)].
	
param_number(_,[],_) -> [];
param_number(TupleSize,[H|T],Count) when is_tuple(H) ->
	Sp = lists:map(fun(X) ->  N = integer_to_binary(X), 
							  <<<<"$">>/binary, N/binary>>   
				   end,
				   lists:seq(Count, Count+TupleSize-1)),
	F1 = fun([X],_) ->
				 [<<X/binary,<<")">>/binary>>];
			([X|XTail],R1) ->  
				 [ << X/binary, <<",">>/binary>> | R1(XTail,R1)]
		 end,
	[ [<<<<"(">>/binary>> | F1(Sp,F1)] | param_number(TupleSize,T,Count+TupleSize)].

setfunc([LastV],Count,Acc,_S) ->
	Last = bin(LastV),
	Cb = integer_to_binary(Count), 
	<<Acc/binary, Last/binary, <<" = $">>/binary, Cb/binary,<<" ">>/binary>>;
setfunc([FV|Rest],Count,Acc,S) ->
	F = bin(FV),
	Cb = integer_to_binary(Count),   
	setfunc(Rest,Count+1,<<Acc/binary,F/binary,<<" = $">>/binary, Cb/binary,S/binary>>,S).	

txtcolumns([Name]) -> [Name];
txtcolumns([Name|Rest]) ->
	[<<Name/binary,<<", ">>/binary>> | txtcolumns(Rest)].


bin({{Y,M,D},{HH,MM,SSf}}) -> 
	SS = trunc(SSf),
	list_to_binary(string:join([string:join(
								  [lists:flatten(io_lib:format("~4..0B",[Y])), 
								   lists:flatten(io_lib:format("~2..0B",[M])), 
								   lists:flatten(io_lib:format("~2..0B",[D]))]
								  ,"-"), 
								string:join([
											 lists:flatten(io_lib:format("~2..0B",[HH])), 
											 lists:flatten(io_lib:format("~2..0B",[MM])), 
											 lists:flatten(io_lib:format("~2..0B",[SS])) 
											],":")]," "));
bin(Val) when is_list(Val) -> list_to_binary(Val);
bin(Val) when is_binary(Val) -> Val;
bin(Val) when is_number(Val) ->
	[V2str] = io_lib:format("~p",[Val]),
	list_to_binary(V2str);
bin(Val) when  is_atom(Val) ->
	[V2str] = io_lib:format("~p",[Val]),
	list_to_binary(V2str).



unl_balance_out(Edition) ->
	case db:equery(db_default,
				   " select edition,
				  db_container_out - mf_container_out,
				  db_sum_out - mf_sum_out
				  from replica.hsv where  db_container_out > mf_container_out and db_sum_out > mf_sum_out and
				   edition = $1",
				   [Edition]) of 
		{ok,_,[{E,C,S}]} ->
			db:equery(db_default,"insert into replica.id_bl_unl(edition,id,hide) select $1::integer,  "
					  "unnest(replica.ccs_out_s1_v2($1::integer,$2::integer,$3::numeric)), 1::integer",
					  [E,C, binary_to_float(S)]);
		{ok, _, []} -> {empty_list, Edition}
	end.

unl_balance_in(Edition) ->
%%	db:equery(db_default,"select replica.unlfun_p1($1)",[Edition]).
	case db:equery(db_default,
				   " select edition,
				  db_container_in - mf_container_in,
				  db_sum_in - mf_sum_in
				  from replica.hsv where  db_container_in > mf_container_in and db_sum_in > mf_sum_in and
				   edition = $1",
				   [Edition]) of 
		{ok,_,[{E,C,S}]} ->
			io:format("~p~n",[{E,C,S}]),
			db:equery(db_default,"insert into replica.id_in_bl(edition,id,hide)  select $1::integer,"
					 " unnest(replica.ccs_in_s1($1::integer, $2::integer, $3::numeric)) , 1::integer",
					  [E,C, binary_to_float(S)]),
			io:format("Next~n",[]);
		{ok, _, []} -> {empty_list, Edition}
	end.


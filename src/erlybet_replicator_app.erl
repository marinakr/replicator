-module(erlybet_replicator_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
	%apputils:ensure_started(inets),
	apputils:ensure_started(logger),
	apputils:ensure_started(dbproxy),
	application:start(mq),
    erlybet_replicator_sup:start_link().

stop(_State) ->
    ok.

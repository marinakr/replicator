-module(erlybet_replicator).

%% API
-export([start/0, stop/0]).

%%%===================================================================
%%% API
%%%===================================================================

start() ->
	application:start(erlybet_replicator).

stop() ->
	application:stop(erlybet_replicator).


-define(DB_PRODUCTION, db_master).
-define(DB_ANALYTIC, db_default).
-define(DB_NOTIFY, pgsql).
-define(SERVER,  ?MODULE).
-define(REPLICA, "erlyrepl").
-define(REPLICSCH, "replica").
-define(MASTER, "master").
-define(SLAVE, "slave").
-define(TGTABLENAME,"TG_TABLE_NAME").
-define(RECORD_INSERTED, 1).
-define(RECORD_UPDATED, 0).
-define(REPLFIELDS,[{"\"status_t\"","integer"},
					%{"\"idn_erlyrepl\"", "bigint"},
					%{"\"del_erlyrepl\"","boolean"},
					{"\"last_update\"","timestamp(6) with time zone"}]).
-define(MAXLIMIT, 30).
-define(REPLICA_DELAY, 5000). %miliseconds ask for any updates/inserts
-define(TIMEOUT_SELECT, 1). %%Timeout in seconds wait for repeat "select"  
-define(REDIS_KEY_INSERT, "replication_rows_insert_").
-define(REDIS_KEY_UPDATE, "replication_rows_update_").


















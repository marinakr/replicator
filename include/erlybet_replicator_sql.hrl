-define(TABLES4REPLICATION,"select tschema,tname from replica.reply_tables where \"active\" = 1").
-define(PK_KEYS," SELECT '\"'||pg_attribute.attname||'\"' "
		  " FROM pg_index, pg_class, pg_attribute "
		  " WHERE  pg_class.oid = $1::regclass AND "
 		  " indrelid = pg_class.oid AND  pg_attribute.attrelid = pg_class.oid AND "
		  " pg_attribute.attnum = any(pg_index.indkey) AND indisprimary").
-define(ASK_MAX_LAST_UPDATE,"select max(last_update) from ").



-define(ERR_CODE_KEY_VIOLATES, <<"23505">>).
-define(ERR_CODE_STATEMENT_TIMEOUT, <<"57014">>).
-define(ERR_CODE_FOREIGN_KEY, <<"23503">>).
-define(ERR_EMPTY, {ok,0}).


-define(ERR_FOREIGN_KEY_VIOLATES, {error,{error,_,?ERR_CODE_FOREIGN_KEY,_,_}}).
-define(ERR_PGSQL_KEY_VIOLATES, {error,{error,_,?ERR_CODE_KEY_VIOLATES,_,_}}).
-define(ERR_PGSQL_STATEMENT_TIMEOUT, {error,{error,_,?ERR_CODE_STATEMENT_TIMEOUT,_,_}}).
-define(ERR_COMM_TIMEOUT,{error,{connect,timeout}}).
-define(ERR_OVERLOAD,{error,{overload,_}}).


